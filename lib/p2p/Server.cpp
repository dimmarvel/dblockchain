//
// Created by root on 19.01.22.
//

#include "Server.h"

namespace p2p {

    Server::Server() {
        _connect = std::make_unique<Connection>();
    }

    void Server::Start() {
        spdlog::info("Server started!");
    }

}
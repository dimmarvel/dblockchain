//
// Created by root on 19.01.22.
//

#ifndef DBLOCKCHAIN_SERVER_H
#define DBLOCKCHAIN_SERVER_H

#include "Connection.h"
#include <memory>

namespace p2p {

    class Server {

    public:
        Server();
        Server(Connection connection);

    public:
        void Start();

    private:
        std::unique_ptr<Connection> _connect;
    };

}


#endif //DBLOCKCHAIN_SERVER_H

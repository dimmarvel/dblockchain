//
// Created by root on 19.01.22.
//

#include "Connection.h"

namespace p2p {

    Connection::Connection() {
        _ip = "127.0.0.1";
        _port = 8080;
        spdlog::info("Connect to local machine: " + _ip + ":" + std::to_string(_port));
        spdlog::info("Connection success!");
    }

    Connection::Connection(std::string ip, int port) {
        _ip = ip;
        _port = port;
        spdlog::info("Connect to: " + _ip + ":" + std::to_string(_port));
        spdlog::info("Connection success!");
    }

}
//
// Created by root on 19.01.22.
//

#ifndef DBLOCKCHAIN_CONNECTION_H
#define DBLOCKCHAIN_CONNECTION_H

#include <iostream>
#include <string>
#include <spdlog/spdlog.h>

namespace p2p {

    class Connection {

    public:
        Connection(std::string ip, int port);
        Connection();

    private:
        std::string _ip;
        int         _port;

    };

}


#endif //DBLOCKCHAIN_CONNECTION_H
